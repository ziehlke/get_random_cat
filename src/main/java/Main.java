import com.google.gson.Gson;
import net.gpedro.integrations.slack.*;
import pl.sda.cat.Cat;

import java.io.*;
import java.net.URL;
import java.util.Properties;

public class Main {

    private static final String LINK = "https://aws.random.cat/meow";

    public static void main(String[] args) throws IOException {
        FileReader fileReader = new FileReader(new File(".secret"));
        Properties secrets = new Properties();
        secrets.load(fileReader);

        String json = readUrl(LINK);
        Gson gson = new Gson();
        SlackApi api = new SlackApi(secrets.getProperty("hook"));
        Cat cat = gson.fromJson(json, Cat.class);

        SlackAttachment slackAttachment = new SlackAttachment("cats rulez!");
        slackAttachment.setImageUrl(cat.getFile());

        SlackAction grinAction = new SlackAction("grin", ":grin:" , SlackActionType.BUTTON, "grin");
        SlackAction minusAction = new SlackAction("-1", ":-1:" , SlackActionType.BUTTON, "-1");

        api.call(new SlackMessage()
                .addAttachments(
                        slackAttachment
                                .addAction(grinAction)
                                .addAction(minusAction)
                                .setCallbackId(""))
                .setText("wincyj kotów"));

        System.out.println("done");
    }



    public static String readUrl(String urlString) throws IOException {
        System.setProperty("http.agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:56.0) Gecko/20100101 Firefox/56.0");

        BufferedReader reader = null;
        try {
            URL url = new URL(urlString);
            reader = new BufferedReader(new InputStreamReader(url.openStream()));
            StringBuffer buffer = new StringBuffer();
            int read;
            char[] chars = new char[1024];
            while ((read = reader.read(chars)) != -1)
                buffer.append(chars, 0, read);
            return buffer.toString();

        } finally {
            if (reader != null)
                reader.close();
        }
    }
}
